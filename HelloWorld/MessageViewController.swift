//
//  MessageViewController.swift
//  HelloWorld
//
//  Created by Sebastian Guerrero F on 6/3/20.
//  Copyright © 2020 SG. All rights reserved.
//

import UIKit

class MessageViewController: UIViewController {

    @IBOutlet weak var messageLabel: UILabel!
    
    var message:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageLabel.text = message
        
        print("VIEW 2 - DID LOAD")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("VIEW 2 - WILL APPEAR")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("VIEW 2 - DID APPEAR")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("VIEW 2 - WILL DISAPPEAR")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("VIEW 2 - DID DISAPPEAR")
    }
}
